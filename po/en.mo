��    F      L  a   |                           &     -     5     C     J     S     b     u     }     �  
   �     �     �  _   �          <     Z     a     g     o     u     z     �     �     �     �     �  	   �     �     �     �     �     �                $     7     ?     N     ^     u     |  !   �     �     �     �  
   �     �     �     
	     )	     C	     _	     |	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
  �  
     �     �     �     �     �     �     �                    .     6     L  
   [     f     l  _   x     �     �                     (     .     3     F     N     \     k     z  	   �     �     �     �     �     �     �     �     �     �     �                '     .  !   C  )   e     �  
   �  
   �     �     �     �     �     	     #     @     F     X     _     d     j     q  	   �     �     �     �     �     �               "   (      &      ,            )       5      /                   0       A   :   C             B   >         6                          !          1   F           $   7             D   -            #   @         
       ?   %   +         4   ;       	   *      2      8                             3   9   =   <   '   E               .           <i>none</i> Adjust the volume Albums Artist Artists Audio Devices Audio: Bitrate: Clear Playlist Clear the playlist Connect Connect or disconnect Connect to MPD Connection Cover Cover Panel CoverGrid is a client for the Music Player Daemon, focusing on albums instead of single tracks. Enter hostname or IP address Enter password or leave blank Error: File: General Host: Info Keyboard Shortcuts Library Library Panel Loading albums Loading images Open the info dialog Password: Play Player Playlist Port: Quit Quit the application Search Library Search the library Seconds Seconds played Seconds running Select multiple albums Server Settings and actions Show the cover in fullscreen mode Show the keyboard shortcuts Songs Sort Statistics Status Switch between play and pause Switch to the Connection panel Switch to the Cover panel Switch to the Library panel Switch to the Playlist panel Title Toggle Fullscreen cancel play queue remove search library sort by artist sort by modification sort by title sort by year {} feat. {} {}:{} minutes Project-Id-Version: CoverGrid (mcg)
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-01-08 19:07+0100
Last-Translator: coderkun <olli@suruatoel.xyz>
Language-Team: 
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.2.2
X-Poedit-Basepath: ../../..
X-Poedit-SearchPath-0: mcg
X-Poedit-SearchPath-1: data/ui
 <i>none</i> Adjust the volume Albums Artist Artists Audio Devices Audio: Bitrate: Clear Playlist Clear the playlist Connect Connect or disconnect Connect to MPD Connection Cover Cover Panel CoverGrid is a client for the Music Player Daemon, focusing on albums instead of single tracks. Enter hostname or IP address Enter password or leave blank Error: File: General Host: Info Keyboard Shortcuts Library Library Panel Loading albums Loading images Open the info dialog Password: Play Player Playlist Port: Quit Quit the application Search Library Search the library Seconds Seconds Seconds running Select multiple albums Server Settings and actions Show the cover in fullscreen mode Show the keyboard shortcuts (this dialog) Songs Sort order Statistics Status Switch between play and pause Switch to the Connection panel Switch to the Cover panel Switch to the Cover panel Switch to the Playlist panel Title Toggle fullscreen cancel play queue remove search library by Artist by Modification Date by Title by Year {} feat. {} {}:{} minutes 