import sys
import gi
gi.require_version('Gtk', '3.0')

from .application import Application



def main(version):
    app = Application()
    return app.run(sys.argv)
