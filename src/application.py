#!/usr/bin/env python3


import logging
import urllib

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gio, Gtk, Gdk, GLib

from .window import Window
from .infodialog import InfoDialog




class Application(Gtk.Application):
    TITLE = "CoverGrid"
    ID = 'xyz.suruatoel.mcg'
    DOMAIN = 'mcg'


    def __init__(self):
        super().__init__(application_id=Application.ID, flags=Gio.ApplicationFlags.FLAGS_NONE)
        self._window = None
        self._info_dialog = None
        self._verbosity = logging.WARNING
        #self.create_action('quit', self.quit, ['<primary>q'])
        #self.create_action('about', self.on_about_action)
        #self.create_action('preferences', self.on_preferences_action)


    def do_startup(self):
        Gtk.Application.do_startup(self)
        self._setup_logging()
        self._load_settings()
        self._set_default_settings()
        self._load_css()
        self._setup_actions()
        self._load_appmenu()


    def do_activate(self):
        Gtk.Application.do_activate(self)
        if not self._window:
            self._window = Window(self, Application.TITLE, self._settings)
        self._window.present()


    def on_menu_info(self, action, value):
        if not self._info_dialog:
            self._info_dialog = InfoDialog()
        self._info_dialog.run()
        self._info_dialog.hide()


    def on_menu_quit(self, action, value):
        self.quit()


    def _setup_logging(self):
        logging.basicConfig(
            level=self._verbosity,
            format="%(asctime)s %(levelname)s: %(message)s"
        )


    def _load_settings(self):
        self._settings = Gio.Settings.new(Application.ID)


    def _set_default_settings(self):
        settings = Gtk.Settings.get_default()
        settings.set_property('gtk-application-prefer-dark-theme', True)


    def _load_css(self):
        styleProvider = Gtk.CssProvider()
        styleProvider.load_from_resource(self._get_resource_path('gtk.css'))
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            styleProvider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )


    def _setup_actions(self):
        action = Gio.SimpleAction.new("info", None)
        action.connect('activate', self.on_menu_info)
        self.add_action(action)
        action = Gio.SimpleAction.new("quit", None)
        action.connect('activate', self.on_menu_quit)
        self.add_action(action)


    def _load_appmenu(self):
        builder = Gtk.Builder()
        builder.set_translation_domain(Application.DOMAIN)
        builder.add_from_resource(self._get_resource_path('ui/gtk.menu.ui'))
        self.set_app_menu(builder.get_object('app-menu'))


    def _get_resource_path(self, path):
        return "/{}/{}".format(Application.ID.replace('.', '/'), path)
