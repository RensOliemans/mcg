# CoverGrid

CoverGrid (mcg) is a client for the [Music Player
Daemon](http://www.musicpd.org) (MPD), focusing on albums instead of single
tracks. It is not intended to be a replacement for your favorite MPD client but
an addition to get a better album-experience.

Website: https://www.suruatoel.xyz/codes/mcg

License: [GPL](http://www.gnu.org/licenses/gpl.html) v3

Dependencies:

* [Python](http://www.python.org) 3
* [python-dateutil](https://pypi.org/project/python-dateutil/)
* [GTK](http://www.gtk.org) 3 (>= 3.22) ([python-gobject](https://live.gnome.org/PyGObject))
* [Avahi](http://www.avahi.org) (optional)
* [python-keyring](http://pypi.python.org/pypi/keyring) (optional)
* [meson](https://mesonbuild.com/) and [ninja](https://ninja-build.org/) (building)
* [gettext](https://www.gnu.org/software/gettext/) (building)

Additionally a [MPD](http://www.musicpd.org) server (version >= 0.21.0) is
required at runtime.


## Building

Build the application with _meson_ and _ninja_:

    $ meson build
    $ ninja -C build

## Running/Testing

For testing the application and running it without (system-wide) installation,
donwload/clone the code, build it with the `--prefix` option and install it
with `ninja`:

    $ meson --prefix $(pwd)/install build
    $ ninja -C build
    $ ninja -C build install

After that you can run it with _meson’s_ `devenv` command:

    $ meson devenv -C build src/mcg

## Installing

Install the application system-wide with _meson_ (after building):

    $ ninja -C build install

Note: _On Linux using the distribution’s package manager is the preferred way
of installing applications system-wide._

## Screenshots

![CoverGrid’s cover panel with album details and track list.](https://suruatoel.xyz/images/mcg-cover-s.png "CoverGrid’s cover panel with album details and track list.")

![CoverGrid’s playlist panel with queued albums.](https://suruatoel.xyz/images/mcg-playlist.png "CoverGrid’s playlist panel with queued albums.")

![CoverGrid’s library panel showing the albums middle-sized.](https://suruatoel.xyz/images/mcg-library-m.png "CoverGrid’s library panel showing the albums middle-sized.")

![CoverGrid’s library panel showing the albums small-sized.](https://suruatoel.xyz/images/mcg-library-s.png "CoverGrid’s library panel showing the albums small-sized.")
